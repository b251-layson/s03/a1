class Camper():
    def __init__(self, name, batch, course):
        self.name = name
        self.batch = batch
        self.course = course

    def career_track(self):
        print(f"Camper Name: {self.name}")
        print(f"Camper Batch: {self.batch}")
        print(f"Camper Course: {self.course}")
        print(f"My name is {self.name} of batch {self.batch}")
        print(f"Currently enrolled in the  {self.course} program")


zuitt_camper = Camper("Alan", 100, "python short course")


zuitt_camper.career_track()